module.exports = function (self) {
  let other = this.HttpMethod;
  return other.method === '*' || self.method === other.method;
};
